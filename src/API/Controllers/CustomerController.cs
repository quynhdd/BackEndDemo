﻿using API.Models;
using API.Repository.Interface;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/customer")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        #region Fields

        private readonly ICustomerRepo _customerRepo;

        #endregion

        #region Ctor

        public CustomerController(ICustomerRepo customerRepo)
        {
            _customerRepo = customerRepo;
        }

        #endregion

        #region Method

        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "id": 1,
        ///         "fullName": "Mr.Bean"
        ///     }
        ///
        /// </remarks>
        [HttpPost]
        public virtual IActionResult InsertCustomer(Customer model)
        {
            if (model == null)
                return BadRequest();

            if (string.IsNullOrEmpty(model.FullName))
                return BadRequest();

            _customerRepo.InsertCustomer(model);

            return Ok();
        }

        /// <summary>
        /// Get all customer
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual IActionResult Get()
        {
            var data = _customerRepo.Get();
            if (data == null)
                return NoContent();

            return Ok(data);
        }

        #endregion
    }
}
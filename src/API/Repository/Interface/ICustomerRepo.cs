﻿using API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Repository.Interface
{
    public interface ICustomerRepo
    {
        /// <summary>
        /// Insert Customer
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        Customer InsertCustomer(Customer item);

        /// <summary>
        /// Get
        /// </summary>
        /// <returns></returns>
        Dictionary<string, Customer> Get();
    }
}

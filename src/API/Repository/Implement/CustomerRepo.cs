﻿using API.Models;
using API.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Repository.Implement
{
    public class CustomerRepo : ICustomerRepo
    {
        #region Fields

        private readonly Dictionary<string, Customer> _customerRepo;

        #endregion

        #region Ctor

        public CustomerRepo()
        {
            _customerRepo = new Dictionary<string, Customer>();
        }

        #endregion

        #region Method

        /// <summary>
        ///  Insert Customer
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public virtual Customer InsertCustomer(Customer item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            _customerRepo.Add(item.FullName, item);

            return item;
        }

        /// <summary>
        /// Get all
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, Customer> Get()
        {
            return _customerRepo;
        }

        #endregion
    }
}
